﻿using Inf1008.Tp1.Core;

namespace Inf1008.Tp1
{
    public class Program
    {
        public static void Main()
        {
            var run = true;
            var labyrinth = new Labyrinth(1, 1);

            while (run)
            {
                Console.Write("Entrez une taille de labyrinthe sous le format \"x y\" (q pour quitter): ");
                var line = Console.ReadLine();

                switch (line)
                {
                    case null:
                    case "":
                        Console.WriteLine("Entrée vide.");
                        break;
                    case "q":
                        run = false;
                        break;
                    default:
                        var sizes = line.Split(' ', 2);

                        if (sizes.Length > 0)
                        {
                            if (sizes.Length > 1)
                            {
                                if (int.TryParse(sizes[0], out var x))
                                {
                                    if (int.TryParse(sizes[1], out var y))
                                    {
                                        labyrinth.Generate(x, y);
                                        Console.WriteLine(labyrinth);
                                        Console.WriteLine(labyrinth.StringCount());
                                    }
                                    else
                                    {
                                        Console.WriteLine("La taille en y doit être un entier.");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("La taille en x doit être un entier.");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Taille en y manquante.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Taille en x et y manquante.");
                        }
                        break;
                }
            }
        }
    }
}

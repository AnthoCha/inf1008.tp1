﻿using System.Collections.Generic;

namespace Inf1008.Tp1.Core
{
    public class LabyrinthNode
    {
        public LabyrinthNode()
        {
            Edges = new List<LabyrinthEdge>();
        }

        public ICollection<LabyrinthEdge> Edges { get; set; }
    }
}

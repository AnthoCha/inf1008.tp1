﻿using System;

namespace Inf1008.Tp1.Core
{
    public class LabyrinthEdge
    {
        public LabyrinthEdge(LabyrinthNode node, int weight)
        {
            Node = node ?? throw new ArgumentNullException(nameof(node));
            Weight = weight;
        }

        public LabyrinthNode Node { get; }

        public int Weight { get; }
    }
}

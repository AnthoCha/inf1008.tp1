﻿using Microsoft.Bcl.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inf1008.Tp1.Core
{
    public class LabyrinthPrim
    {
        public IEnumerable<LabyrinthEdge> Solve(LabyrinthNode root, int nodesCount)
        {
            if (root == null)
            {
                throw new ArgumentNullException(nameof(root));
            }

            if (nodesCount < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(nodesCount));
            }

            var nodes = new HashSet<LabyrinthNode>
            {
                root
            };

            LabyrinthEdge edge;

            // LabyrinthPrim.Solve is O(n^2) = O(n - 1) * O(n)
            // while loop is O(n - 1) where n is nodesCount
            while (nodes.Count < nodesCount)
            {
                // inner loop is O(n) where n is the number of edges for all already selected nodes
                edge = nodes
                    // Enumerable<T>.SelectMany is a deferred execution, the selector function will be called only when the sequence is enumerated
                    .SelectMany(start => start.Edges)
                    // Enumerable<T>.Where is a deferred execution, the predicate function will be called only when the sequence is enumerated
                    .Where(e => !nodes.Contains(e.Node)) // HashSet<T>.Contains implements an hash table lookup which is O(1)
                    // Enumerable<T>.MinBy is O(n) as it enumerates all edges once
                    .MinBy(availableEdge => availableEdge.Weight);

                nodes.Add(edge.Node);
                yield return edge;
            }
        }
    }
}

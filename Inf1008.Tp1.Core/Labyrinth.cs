﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Inf1008.Tp1.Core
{
    public class Labyrinth
    {
        private readonly LabyrinthPrim _prim;
        private readonly Random _random;
        private LabyrinthNode[,] _nodes;
        private char[,] _labyrinth;
        private IDictionary<LabyrinthEdge, Point> _edgePoints;
        private int _opCountIni = 0;
        private int _opCountGen = 0;
        private int _opCountAff = 0;

        public Labyrinth(int x, int y)
        {
            _prim = new LabyrinthPrim();
            _random = new Random();
            Generate(x, y);
        }

        public void Generate(int x, int y)
        {
            _opCountIni = 0;
            _opCountGen = 0;
            _opCountAff = 0;

            if (x < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(x));
            }

            if (y < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(y));
            }

            _nodes = new LabyrinthNode[x, y];
            _labyrinth = new char[x * 2 - 1, y * 2 - 1];
            _edgePoints = new Dictionary<LabyrinthEdge, Point>();
            _opCountIni += 3;

            // First row and column has no top edge and no left edge
            GenerateNode(0, 0);
            _opCountGen++;

            // First column only has a top edge and no left edge
            for (var i = 1; i < x; i++)
            {
                GenerateNode(i, 0);
                GenerateTopEdge(i, 0);
                _opCountGen += 3;
            }

            // First row only has a left edge and no top edge
            for (var j = 1; j < y; j++)
            {
                GenerateNode(0, j);
                GenerateLeftEdge(0, j);
                _opCountGen += 3;
            }

            // All other rows and columns have a top edge and a left edge
            for (var i = 1; i < x; i++)
            {
                for (var j = 1; j < y; j++)
                {
                    GenerateNode(i, j);
                    GenerateTopEdge(i, j);
                    GenerateLeftEdge(i, j);
                    _opCountGen += 5;

                    // Top-left diagonal has no edge and should always be marked with an X
                    _labyrinth[i * 2 - 1, j * 2 - 1] = 'X';
                    _opCountAff += 3;
                }
            }

            _labyrinth[0, 0] = 'S';
            _labyrinth[x * 2 - 2, y * 2 - 2] = 'E';
            _opCountAff += 2;

            var selectedEdges = _prim.Solve(_nodes[0, 0], _nodes.Length);
            _opCountAff++;

            foreach (var selectedEdge in selectedEdges)
            {
                var edgePoint = _edgePoints[selectedEdge];
                _labyrinth[edgePoint.X, edgePoint.Y] = 'O';
                _opCountAff +=3;
            }
        }

        private void GenerateNode(int i, int j)
        {
            _nodes[i, j] = new LabyrinthNode();
            _labyrinth[i * 2, j * 2] = 'O'; 
        }

        private void GenerateTopEdge(int i, int j)
        {
            GenerateEdge(_nodes[i, j], _nodes[i - 1, j], new Point(i * 2 - 1, j * 2));
        }

        private void GenerateLeftEdge(int i, int j)
        {
            GenerateEdge(_nodes[i, j], _nodes[i, j - 1], new Point(i * 2, j * 2 - 1));
        }

        private void GenerateEdge(LabyrinthNode firstNode, LabyrinthNode secondNode, Point edgePoint)
        {
            var weight = _random.Next(0, 10);
            var fromFirstToSecondEdge = new LabyrinthEdge(secondNode, weight);
            var fromSecondToFirstEdge = new LabyrinthEdge(firstNode, weight);

            firstNode.Edges.Add(fromFirstToSecondEdge);
            secondNode.Edges.Add(fromSecondToFirstEdge);

            _labyrinth[edgePoint.X, edgePoint.Y] = 'X';
            _edgePoints[fromFirstToSecondEdge] = _edgePoints[fromSecondToFirstEdge] = edgePoint;
        }

         public string StringCount()
        {
            string count = "Nombre d'opérations élémentaires :" +
                "\n Initialisation : " + _opCountIni +
                "\n Génération : " + _opCountGen +
                "\n Lecture/Affichage : " + _opCountAff + "\n";

            return count;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            var x = _labyrinth.GetLength(0);
            var y = _labyrinth.GetLength(1);

            for (var i = 0; i < x; i++)
            {
                for (var j = 0; j < y; j++)
                {
                    sb.Append(_labyrinth[i, j]);
                    sb.Append(' ');
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
